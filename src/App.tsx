import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import MovieList from './components/MovieList'
import { createTheme, ThemeProvider } from '@mui/material/styles';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

function App() {


  return (
    <ThemeProvider theme={darkTheme}>
    <div className="App">
      <h1>coding test</h1>
        <MovieList />
    </div>
    </ThemeProvider>
  )

}

export default App
