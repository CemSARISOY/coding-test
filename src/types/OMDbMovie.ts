export interface OMDbMovie {
    Poster: string,
    Title: string,
    Type: string,
    Year: number,
    imdbID: string
}

export interface OMDbMovieDetailled {
    Title: string,
    Year: number,
    Rated: string,
    Released: Date,
    Runtime: string,
    Genre: string,
    Director: string,
    Writer: string,
    Actors: string,
    Plot: string,
    Language: string,
    Country: string,
    Awards: string,
    Poster: string,
    Ratings: rating[],
    Metascore: string,
    imdbRating: string,
    imdbVotes: string,
    imdbID: string,
    Type: string,
    DVD: string,
    BoxOffice: any,
    Production: any,
    Website: string,
}

interface rating {
    Source: string,
    Value: string
}