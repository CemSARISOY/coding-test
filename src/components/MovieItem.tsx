import Button from "@mui/material/Button"
import Modal from "@mui/material/Modal"
import TableCell from "@mui/material/TableCell"
import TableRow from "@mui/material/TableRow"
import Typography from "@mui/material/Typography"
import { Box } from "@mui/system"
import axios from "axios"
import { useState } from "react"
import { OMDbMovie, OMDbMovieDetailled } from "../types/OMDbMovie"

function MovieItem({movie} : {movie: OMDbMovie}) {

    const [details, setDetails] = useState<OMDbMovieDetailled>()
    const [open, setOpen] = useState<boolean>(false)

    const fetchDetails = () => {
        axios.get(`http://www.omdbapi.com/?apikey=d37dede0&i=${movie.imdbID}`)
        .then( (resp) => {
            console.log(resp);
            if(resp.data.Response === "True"){
                setDetails({...resp.data} as OMDbMovieDetailled)
                setOpen(true)
            }
        } )
    }

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <TableRow>
            <TableCell>{movie.Title}</TableCell>
            <TableCell>{movie.Year}</TableCell>
            <TableCell>
                {
                    movie.Poster !== "N/A" ?
                        <img src={movie.Poster} width="100px"/>
                    :
                        "Not available"
                }
            </TableCell>
            <TableCell>
                <Button onClick={fetchDetails}>
                    Show details
                </Button>
                <Modal
                    open={open}
                    onClose={handleClose}
                    >
                    <Box>
                        <Typography variant="h6" component="h2">
                        Information about the movie {movie.Title}
                        </Typography>
                        <Typography >
                            <p>Plot : {details?.Plot}</p>
                            <p>Runtime : {details?.Runtime}</p>
                            <p>Genre : {details?.Genre}</p>
                            <p>Rating : {details?.Ratings.map( (rating) => ("{" + rating.Source + ", " + rating.Value + "} /"))}</p>
                        </Typography>
                    </Box>
                </Modal>
            </TableCell>
        </TableRow>

    )
  }
  
export default MovieItem