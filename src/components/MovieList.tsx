import { TextField } from "@mui/material"
import Button from "@mui/material/Button"
import Pagination from "@mui/material/Pagination";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import axios from "axios"
import { useEffect, useState } from "react";
import { OMDbMovie } from "../types/OMDbMovie";
import MovieItem from "./MovieItem";

function MovieList() {

    const [searchValue, setSearchValue] = useState<string>("")
    const [movieList, setMovieList] = useState<OMDbMovie[]>([])
    const [numberResults, setNumberResults] = useState<number>(0)
    const [page, setPage] = useState<number>(1)
    const [error, setError] = useState<string>("")


    const searchByTitle = () => {
        axios.get(`http://www.omdbapi.com/?apikey=d37dede0&s=${encodeURI(searchValue.replace(" ", "+"))}&page=${page}`)
        .then( (resp) => {
            console.log(resp);
            if(resp.data.Response === "True"){
                setNumberResults(resp.data.totalResults)
                let list : OMDbMovie[] = []
                for(let i = 0 ; i < resp.data.Search.length ; i++){
                    list.push({...resp.data.Search[i]} as OMDbMovie)
                }
                setMovieList(list)
                setError("");
            }
            else{
                setError(resp.data.Error)
                setMovieList([])
            }
        } )
    }

    const handleSearch = (value : string) => {
        setSearchValue(value)
    };
    
    const handleKey = (e : React.KeyboardEvent<HTMLDivElement>) => {
        if(e.keyCode == 13){
            searchByTitle()
         }
    }

    const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    };

    
    useEffect( () => {
        searchByTitle()
    }, [page])
    
  
    return (
        <div>
            <div className="search" style={{display: "flex"}}>
                <TextField id="outlined-basic" label="Movie title" onKeyDown={ (e) => handleKey(e)} variant="outlined" onChange={(e) => {handleSearch(e.target.value)}}/>
                <Button style={{marginLeft: "1%"}} variant="outlined" onClick={searchByTitle}>Search</Button>
            </div>
            { error && <span style={{color: 'red'}}>{error}</span>}
            <div className="list">
                <h3>List</h3>

                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Title</TableCell>
                                <TableCell>Release Year</TableCell>
                                <TableCell align="center">Poster</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {
                            movieList.map( (movie) => 
                                <MovieItem key={movie.imdbID} movie={movie}/>
                            )
                        }
                        </TableBody>
                    </Table>
                </TableContainer>
                <Pagination count={Math.trunc(numberResults/10)+1} page={page} onChange={handlePageChange}/>
            </div>
        </div>
    )
  }
  
export default MovieList